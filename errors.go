package k

import (
	"bufio"
)

// See Dave Cheney's blog about reducing error handling
type errReadWriter struct {
	bufio.ReadWriter
	err error
}

func (e *errReadWriter) Write(buf []byte) (int, error) {
	if e.err != nil {
		return 0, e.err
	}

	var n int
	n, e.err = e.Writer.Write(buf)
	return n, nil
}

func (e *errReadWriter) Read() (byte, error) {
	if e.err != nil {
		return byte(0), e.err
	}

	var c byte
	c, e.err = e.ReadWriter.ReadByte()
	return c, nil
}
