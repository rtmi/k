package k

import (
	"strings"
)

type erow struct {
	size   int
	rsize  int
	chars  string
	render string
}

func (e *Editor) rowCxToRx() int {
	// convert chars index to render index
	// (i.e., tabs)

	var (
		r  = e.row[e.cy]
		rx = 0
		ch = []rune(r.chars)
	)

	for j := 0; j < e.cx; j++ {
		if ch[j] == '\t' {
			rx += (KILO_TAB_STOP - 1) - (rx % KILO_TAB_STOP)
		}

		rx++
	}

	return rx
}

func (e *Editor) updateRow(y int) {
	var r = e.row[y]

	// transform row's chars into their presentation form
	// - tabs into spaces
	e.row[y].render = strings.ReplaceAll(r.chars, "\t", strings.Repeat(" ", KILO_TAB_STOP))

	e.row[y].rsize = len(e.row[y].render)
}

// appendRow adds the line (to our data model)
func (e *Editor) appendRow(line string) {
	// size is the length of the line (treating as slice of runes / unicode points)
	// chars is the text content of the line
	var r = erow{
		size:   len([]rune(line)),
		rsize:  0,
		chars:  line,
		render: "",
	}

	var y = e.rowcount()
	e.row = append(e.row, r)
	e.updateRow(y)
}

func (e *Editor) rowInsertChar(at int, c rune) {
	var r = e.row[e.cy]
	var i = at
	// Check insertion point is within slice
	if at < 0 || at > r.size {
		i = r.size
	}

	// insert rune at position i
	var s = []rune(r.chars)
	s = append(s, 0)
	copy(s[i+1:], s[i:])
	s[i] = c
	e.row[e.cy].chars = string(s)

	// sync the insert result
	e.row[e.cy].size = len(s)
	e.updateRow(e.cy)
}

// rowcount is the total lines of the file that was opened and read
func (e *Editor) rowcount() int {
	// todo maybe rename e.row
	return len(e.row)
}
