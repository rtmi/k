package k

import (
//"unicode"
)

const (
	BACKSPACE rune = 127
	ARROW_LT  rune = 1000 + iota
	ARROW_RT
	ARROW_UP
	ARROW_DN
	DEL_KEY
	HOME_KEY
	END_KEY
	PAGE_UP
	PAGE_DOWN
)

// ProcessKeypress returns true when quit cmd rcvd.
func (e *Editor) ProcessKeypress(errc chan<- error) bool {
	var c = e.readKey(errc)

	switch c {
	case '\r':
		// TODO
		break

	case rune(CTRL_Q):
		e.vt100cmd("CLR", errc)
		e.vt100cmd("TOP", errc)
		e.rw.Flush()
		return true

	case rune(CTRL_S):
		e.Save()

	case HOME_KEY:
		e.cx = 0
	case END_KEY:
		if e.cy < e.rowcount() {
			e.cx = e.row[e.cy].size
		}

	case BACKSPACE, rune(CTRL_H),
		DEL_KEY:
		// TODO
		break

	case PAGE_UP,
		PAGE_DOWN:
		var a = ARROW_DN
		if c == PAGE_UP {
			a = ARROW_UP
			e.cy = e.rowoff
		} else if c == PAGE_DOWN {
			e.cy = e.rowoff + e.screenrows - 1
			if e.cy > e.rowcount() {
				e.cy = e.rowcount()
			}
		}

		for i := e.screenrows; i != 0; i-- {
			e.moveCursor(a)
		}

	case ARROW_UP,
		ARROW_DN,
		ARROW_LT,
		ARROW_RT:
		e.moveCursor(c)

	case '\x1b', rune(CTRL_L):
		// TODO
		break

	default:
		e.insertChar(c)
	}

	return false
}

func (e *Editor) moveCursor(c rune) {

	switch c {
	case ARROW_LT:
		if e.cx != 0 {
			e.cx--
		} else if e.cy != 0 {
			e.cy--
			e.cx = e.row[e.cy].size
		}
	case ARROW_RT:
		if e.cy < e.rowcount() {
			var r = e.row[e.cy]
			if e.cx < r.size {
				e.cx++
			} else if e.cx == r.size {
				e.cy++
				e.cx = 0
			}
		}
	case ARROW_UP:
		if e.cy != 0 {
			e.cy--
		}
	case ARROW_DN:
		if e.cy < e.rowcount() {
			e.cy++
		}
	}

	// Snap cursor to end of line
	var sz = 0
	if e.cy < e.rowcount() {
		sz = e.row[e.cy].size
	}
	if e.cx > sz {
		e.cx = sz
	}
}
