package k

import (
	"fmt"

	"os"
	"os/signal"
	"syscall"

	"github.com/pkg/errors"
	"github.com/pkg/term"
)

// rawMode starts terminal in raw mode
func (e *Editor) rawMode(name string) (*term.Term, error) {
	ignoreInterrupt()

	// Open device, enter raw mode and disable flow control
	return term.Open(name, term.RawMode, term.FlowControl(term.NONE))
}

// ignoreInterrupt suppresses control keys
func ignoreInterrupt() {
	// Disable Ctrl-C, Ctrl-Z, Ctrl-S, Ctrl-Q, Ctrl-V, Ctrl-O
	signal.Ignore(os.Interrupt,
		syscall.SIGTSTP,
		syscall.SIGSTOP,
		syscall.SIGCHLD,
		syscall.SIGTTOU,
		syscall.SIGTERM)
}

// readKey returns the key input from the terminal
func (e *Editor) readKey(errc chan<- error) rune {
	var (
		c   byte
		seq = make([]byte, 3)
		err error
	)
	c = e.read(errc)

	if c != '\x1b' {
		return rune(c)
	}

	// Check potential escape sequence

	if seq[0], err = e.rw.ReadByte(); err != nil {
		return '\x1b'
	}
	if seq[1], err = e.rw.ReadByte(); err != nil {
		return '\x1b'
	}

	if seq[0] == '[' {
		if seq[1] >= '0' && seq[1] <= '9' {
			if seq[2], err = e.rw.ReadByte(); err != nil {
				return '\x1b'
			}
			if seq[2] == '~' {
				switch seq[1] {
				case 1:
					return HOME_KEY
				case 3:
					return DEL_KEY
				case 4:
					return END_KEY
				case 5:
					return PAGE_UP
				case 6:
					return PAGE_DOWN
				case 7:
					return HOME_KEY
				case 8:
					return END_KEY
				}
			}
		} else {
			switch seq[1] {
			case 'A':
				return ARROW_UP
			case 'B':
				return ARROW_DN
			case 'C':
				return ARROW_RT
			case 'D':
				return ARROW_LT
			case 'H':
				return HOME_KEY
			case 'F':
				return END_KEY
			}
		}
	} else if seq[0] == 'O' {
		switch seq[1] {
		case 'H':
			return HOME_KEY
		case 'F':
			return END_KEY
		}
	}
	return '\x1b'
}

// cursorPosition calculates the cursor position
func (e *Editor) cursorPosition() (rows, cols int, err error) {
	// Query the terminal status
	if _, err = e.t.Write([]byte(vt100["STAT"])); err != nil {
		return
	}

	var (
		n   int
		buf []byte
		c   = make([]byte, 1)
	)
	// Read the status report
	for i := 0; i < 32; i++ {
		_, err = e.rw.Read(c)
		if err != nil {
			break
		}
		if c[0] == 'R' {
			break
		}
		buf = append(buf, c...)
	}

	// Parse digits from the result
	if buf[0] != '\x1b' || buf[1] != '[' {
		err = errors.New("cursorPosition cursor status has unknown escape prefix")
		return
	}
	// Expected format is "rows;cols"
	n, err = fmt.Sscanf(string(buf[2:]), "%d;%d", &rows, &cols)
	if n != 2 || err != nil {
		err = errors.New("cursorPosition cursor status row;col not matching")
		return
	}

	return
}

// windowSize obtains the height and width of the terminal
func (e *Editor) windowSize() (rows, cols int, err error) {
	// Ask for winsz from system ioctl
	rows, cols, err = e.t.Winsize()
	if err != nil {
		// Place the cursor at the bottom right corner
		if _, err = e.t.Write([]byte(vt100["XYMAX"])); err != nil {
			return
		}
		// Fallback to calculating the winsz
		rows, cols, err = e.cursorPosition()
		return
	}

	return
}

// read wraps error checking with the read-byte
func (e *Editor) read(errc chan<- error) byte {
	c, err := e.rw.ReadByte()
	if err != nil {
		errc <- err
	}
	return c
}
