package k

import (
	"bufio"
	"log"
	"os"
	"strings"

	"github.com/pkg/errors"
)

// open file to edit
func (e *Editor) Open(filename string) error {
	var (
		f   *os.File
		err error
	)
	e.filename = filename
	if f, err = os.Open(filename); err != nil {
		return err
	}
	defer f.Close()

	var s = bufio.NewScanner(f)
	for s.Scan() {
		e.appendRow(s.Text())
	}

	return s.Err()
}

// rowsToString convert row data model into one string
func (e *Editor) rowsToString() (string, error) {
	var (
		b   strings.Builder
		err error
	)
	for _, v := range e.row {
		if _, err = b.WriteString(v.chars); err != nil {
			return "", errors.Wrap(err, "rowsToString builder failed")
		}
		b.WriteRune('\n')
	}
	return b.String(), nil
}

func (e *Editor) Save() {
	if e.filename == "" {
		return
	}
	var (
		err error
		buf string
		f   *os.File
	)
	buf, err = e.rowsToString()
	if err != nil {
		log.Printf("Save failed: %v", err)
		return
	}
	f, err = os.Create(e.filename)
	if err != nil {
		log.Printf("Save failed to open: %v", err)
		return
	}
	defer f.Close()
	_, err = f.WriteString(buf)
	if err != nil {
		log.Printf("Save failed to write: %v", err)
		return
	}
	f.Sync()
}
