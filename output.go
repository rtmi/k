package k

import (
	"fmt"
	"strings"
	"time"
)

// RefreshScreen makes one sweep to change the screen line by line
func (e *Editor) RefreshScreen(errc chan<- error) {
	e.scroll()

	// Prevent cursor blink
	e.vt100cmd("HIDCURS", errc)

	// Position cursor at top
	e.vt100cmd("TOP", errc)

	e.drawRows(errc)
	e.drawStatusBar(errc)
	e.drawMessageBar(errc)

	var xy = fmt.Sprintf(vt100["MOVCURS"],
		(e.cy-e.rowoff)+1,
		(e.rx-e.coloff)+1)
	e.writef(xy, errc)

	// Show cursor
	e.vt100cmd("SHWCURS", errc)

	e.rw.Flush()
}

func (e *Editor) SetStatus(format string, args ...interface{}) {
	e.status = fmt.Sprintf(format, args...)
	e.statusTime = time.Now()
}

func (e *Editor) drawRows(errc chan<- error) {
	// Iterate each row of the screen height
	for y := 0; y < e.screenrows; y++ {
		// Shift the viewable position by row offset
		var filerow = y + e.rowoff

		if filerow < e.rowcount() {
			e.drawText(filerow, errc)
		} else {
			// Draw empty for position past the file content
			e.fillEmptyScr(y, errc)
		}

		// Erase right of cursor
		e.vt100cmd("ERASE", errc)

		e.writef("\r\n", errc)
	}
}

func (e *Editor) drawStatusBar(errc chan<- error) {
	// Inverse color
	e.vt100cmd("INVCOLR", errc)

	var fn = e.filename
	if e.filename == "" {
		fn = "[No Name]"
	}
	var status = fmt.Sprintf("%.20s - %d lines", fn, e.rowcount())

	var num = fmt.Sprintf("%d/%d", e.cy+1, e.rowcount())

	var blanks = strings.Repeat(" ", e.screencols-len(status)-len(num))

	e.writef(status+blanks+num, errc)

	// Regular color
	e.vt100cmd("REGCOLR", errc)
	e.writef("\r\n", errc)
}

func (e *Editor) drawMessageBar(errc chan<- error) {
	e.vt100cmd("ERASE", errc)
	// Only display messages within 5 sec.
	if time.Now().Sub(e.statusTime) > (time.Second * 5) {
		return
	}
	var msg = []rune(e.status)
	var sz = len(msg)
	if sz > e.screencols {
		e.writef(string(msg[0:e.screencols]), errc)
	} else {
		e.writef(e.status, errc)
	}
}

// scroll tracks the offsets to position the viewport
func (e *Editor) scroll() {
	e.rx = 0
	if e.cy < e.rowcount() {
		e.rx = e.rowCxToRx()
	}

	if e.cy < e.rowoff {
		e.rowoff = e.cy
	}
	if e.cy >= e.rowoff+e.screenrows {
		e.rowoff = e.cy - e.screenrows + 1
	}
	if e.rx < e.coloff {
		e.coloff = e.rx
	}

	if e.rx >= e.coloff+e.screencols {
		e.coloff = e.rx - e.screencols + 1
	}
}

func (e *Editor) drawText(filerow int, errc chan<- error) {
	var (
		line = e.row[filerow]
		tx   = []rune(line.render)
		sz   = e.calcTextSize(line.rsize)
	)

	switch sz {
	case 0:
		e.writef("", errc)
	case -1:
		e.writef(string(tx[e.coloff:]), errc)
	default:
		e.writef(string(tx[e.coloff:sz]), errc)
	}
}

// calcTextSize indicates how to treat the viewable row text
func (e *Editor) calcTextSize(sz int) int {
	var (
		rt = sz - e.coloff
		xt = e.screencols + e.coloff
	)

	if rt < 0 {
		return 0
	}

	// With the offset, the remaining text row will fit in the viewport.
	if rt <= e.screencols {
		return -1
	}

	// Otherwise:
	// Calculate additional chars offscreen to the right
	// that doesn't exceed the size of the text row.

	// Check that the size of remaining text doesn't extend offscreen
	// beyond n (where n is offset value)
	if rt < xt {
		return rt
	}

	return xt
}

func (e *Editor) fillEmptyScr(y int, errc chan<- error) {
	if e.rowcount() == 0 && y >= e.rowcount() {
		if y == e.screenrows/3 {
			e.welcomeMessage(errc)
		} else {
			// Tilde along left edge
			e.writef("~", errc)
		}
	}
}

func (e *Editor) welcomeMessage(errc chan<- error) {
	var wm = fmt.Sprintf("Kilo editor -- version %s", KILO_VERSION)

	if len(wm) > e.screencols {
		// truncate
		wm = wm[0:e.screencols]
	}

	var padding = (e.screencols - len(wm)) / 2
	if padding > 0 {
		e.writef("~", errc)
		padding--
	}
	var pleft = strings.Repeat(" ", padding)
	e.writef(pleft, errc)

	e.writef(wm, errc)
}

func (e *Editor) vt100cmd(cmd string, errc chan<- error) {
	_, err := e.rw.WriteString(vt100[cmd])
	if err != nil {
		errc <- err
	}
}

// todo take the specifier as arg
func (e *Editor) writef(m string, errc chan<- error) {
	_, err := e.rw.WriteString(m)
	if err != nil {
		errc <- err
	}
}

func (e *Editor) Close() {
	e.t.Restore()
	e.t.Close()
}
