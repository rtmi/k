package k

var (

////VT100_TLD []byte = []byte("~\r\n")
)

// vt100 holds the terminal commands
var vt100 = map[string]string{
	"CLR":     "\x1b[2J",
	"ERASE":   "\x1b[K",
	"TOP":     "\x1b[H",
	"XYMAX":   "\x1b[999C\x1b[999B",
	"STAT":    "\x1b[6n",
	"HIDCURS": "\x1b[?25l",
	"SHWCURS": "\x1b[?25h",
	"MOVCURS": "\x1b[%d;%dH",
	"INVCOLR": "\x1b[7m",
	"REGCOLR": "\x1b[m",
}
