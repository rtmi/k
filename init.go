package k

import (
	"bufio"
	"time"

	"github.com/pkg/term"
)

type Editor struct {
	t  *term.Term
	rw *bufio.ReadWriter

	cx, cy     int
	rx         int
	rowoff     int
	coloff     int
	screenrows int
	screencols int
	row        []erow
	filename   string
	status     string
	statusTime time.Time
}

const (
	KILO_VERSION  = "0.0.1"
	KILO_TAB_STOP = 8
)

func InitEditor(name string) (*Editor, error) {
	var (
		e   = &Editor{cx: 0, cy: 0, rx: 0, rowoff: 0, coloff: 0}
		err error
	)

	if e.t, err = e.rawMode(name); err != nil {
		return &Editor{}, err
	}

	e.rw = bufio.NewReadWriter(
		bufio.NewReaderSize(e.t, 1),
		bufio.NewWriter(e.t),
	)

	if e.screenrows, e.screencols, err = e.windowSize(); err != nil {
		return &Editor{}, err
	}

	// Reserve bottom as status row
	e.screenrows -= 2

	return e, nil
}
