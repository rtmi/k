package main

import (
	"flag"
	"log"

	"github.com/patterns/k"
)

func main() {
	var (
		device   string
		filename string
		ed       *k.Editor
		err      error
	)
	flag.StringVar(&device, "device", "/dev/tty", "Path to terminal device for open")
	flag.StringVar(&filename, "filename", "", "Path to file for edit")
	flag.Parse()

	if ed, err = k.InitEditor(device); err != nil {
		panic(err)
	}
	defer ed.Close()

	if filename != "" {
		if err = ed.Open(filename); err != nil {
			panic(err)
		}
	}
	ed.SetStatus("HELP: Ctrl-Q = quit")
	var quit = make(chan bool, 1)
	var errc = make(chan error)

	// Spawn goroutine to handle input events.
	go func() {
		defer close(errc)
		defer close(quit)
		for halt := false; !halt; {
			ed.RefreshScreen(errc)
			halt = ed.ProcessKeypress(errc)
		}

		// Signal that quit cmd was rcvd.
		quit <- true
	}()

	select {
	case err = <-errc:
		if err != nil {
			// TODO which errors are fatal, cancel goroutine.
			log.Println(err)
		}
	case <-quit:
		log.Println("Shutdown")
	}
}
