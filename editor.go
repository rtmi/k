package k

func (e *Editor) insertChar(c rune) {
	if e.cy == e.rowcount() {
		e.appendRow("")
	}

	e.rowInsertChar(e.cx, c)
	e.cx++
}
