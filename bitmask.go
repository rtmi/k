package k

type Bitmask byte

const (
	CTRL_KEY Bitmask = 0x1f
	CTRL_Q   Bitmask = 0x11
	CTRL_H   Bitmask = 0x08
	CTRL_L   Bitmask = 0x0C
	CTRL_S   Bitmask = 0x13
)

func (f *Bitmask) HasFlag(flag Bitmask) bool { return *f&flag != 0 }
func (f *Bitmask) AddFlag(flag Bitmask)      { *f |= flag }
