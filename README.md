# k

Always wondered what it takes to make Vi(m).
 [Build Your Own Text Editor](https://viewsourcecode.org/snaptoken/kilo/index.html)
 looks like a great intro, and has very helpful explanation of the code.


## Credits

[Build Your Own Text Editor](https://viewsourcecode.org/snaptoken/kilo/index.html)

kilo-in-go
 by [bediger4000](https://github.com/bediger4000/kilo-in-go)

kilo
 by [antirez](https://github.com/antirez/kilo)

ttyutils
 by [burke](https://github.com/burke/ttyutils/blob/master/ttyutils.go)

syscall.TIOCGWINSZ explained
 by [man page](http://man7.org/linux/man-pages/man4/tty_ioctl.4.html)

