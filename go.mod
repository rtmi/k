module github.com/patterns/k

go 1.13

require (
	github.com/pkg/errors v0.8.1
	github.com/pkg/term v0.0.0-20190109203006-aa71e9d9e942
	golang.org/x/sys v0.0.0-20191210023423-ac6580df4449 // indirect
)

replace github.com/pkg/term => github.com/patterns/term v0.0.0-20191214133121-f34c40c73ac6
